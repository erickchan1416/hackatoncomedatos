import { StringValidationsController } from './../utils/validations/strings.validations';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { ERRPLATFORM, ERRPERMITS } from './../utils/constants/errors.constats';
import { MessagesController } from './../utils/messages/messages';
import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class CallService {
    constructor(private callNumber: CallNumber,
                private platform: Platform,
                private messagesCtrl: MessagesController,
                private validationsCtrl: StringValidationsController) {}

    callNow(numberPhone) {
        let number: string = numberPhone;

        if (number === null || number === undefined || number === '') {
            console.log('Caracter Vacio');
            return;
        }

        if (!this.validationsCtrl.withOutSpaces(number)) {
            console.log('Este no es un número valido');
            return;
        }

        if (!this.validationsCtrl.isNumber(number)) {
            console.log('Este string contiene letras');
            return;
        }

        if (!this.platform.is('cordova')) {
          this.messagesCtrl.presentAlertOk('Error 5601', ERRPLATFORM[5601].toString(), 'Entendido');
          return;
        }
        this.callNumber.callNumber(number, true)
        .then(res => {
          console.log('Launched dialer!', res);
          this.messagesCtrl.presentSmallToast(`Ejecutando llamada a ${number}`);
        })
        .catch(err => {
          console.log('Error launching dialer', err);
          this.messagesCtrl.presentLongToast(`${ERRPERMITS[2001].toString()}`);
        });
      }
}
