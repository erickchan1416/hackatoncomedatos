import { IMarker } from 'src/utils/interfaces/map/marker.interface';
import { MessagesController } from './../../utils/messages/messages';
import { Situation } from './../../utils/interfaces/modules/situation.interface';
import { SituationRepository } from './../../repositories/modules/situation.repository';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class SituationService {
    markers: Observable<IMarker[]>;

    constructor(private _SituationRepository: SituationRepository,
                private _messagesCtrl: MessagesController ) {
        console.log('Situation Service Ready');
    }

    getSituations(): Observable<Situation[]> {
        return this._SituationRepository.getSituations();
    }


    async addSituation(situation: Situation) {
        console.log('Añadiendo desde el servicio');
        return new Promise( async (resolve, reject) => {
            this._messagesCtrl.presentLoading('Creando situación...');
            const { lat, lng } = situation;

            if (lat ===  0 || lng === 0) {
                this._messagesCtrl.hideLoader().then(
                    res => this._messagesCtrl.presentAlertOk('', 'Las coordenadas ingresadas son invalidas.')
                );
                return resolve(false);
            }

            this._SituationRepository.addSituation(situation).then(
                data => {
                    console.log('data add', data);
                    this._messagesCtrl.hideLoader().then(
                        res => {
                            this._messagesCtrl.presentAlertOkPromise('', 'Situación creada con exito').then(
                                (r) => {
                                    if (r) {
                                        console.log('Respuesta', r);
                                        return resolve(true);
                                    }
                                }
                            );
                        }
                    );
                },
                err => {
                    this._messagesCtrl.hideLoader().then(
                        res => {
                            this._messagesCtrl.presentAlertOkPromise('¡Ops!', 'Ocurrió un error al procesar la solicitud').then(
                                (r) => {
                                    if (r) {
                                        return resolve(false);
                                    }
                                }
                            );
                        }
                    );
                }
            ).catch(
                err => {
                    this._messagesCtrl.hideLoader().then(
                        res => {
                            this._messagesCtrl.presentAlertOkPromise('Error', 'Necesitas conexión a internet para procesar esta solicitud').then(
                                (r) => {
                                    if (r) {
                                        console.log('Respuesta', r);
                                        return reject();
                                    }
                                }
                            );
                        }
                    );
                }
            );

        });
    }

}
