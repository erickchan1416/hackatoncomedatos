import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Componente } from '../utils/interfaces/component.interface';
import { IStyleMap } from 'src/utils/interfaces/map/stylemap.interface'; 

@Injectable({
    providedIn: 'root'
})

export class DataService {

    constructor( private http: HttpClient ) { }

    getMenuOpts() {
        return this.http.get<Componente[]>('/assets/data/menuNative.json');
    }

    getStyleMap() {
        return this.http.get<IStyleMap[]>('../assets/data/styleMap.json');
    }
}
