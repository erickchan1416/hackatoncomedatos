import { Injectable } from '@angular/core';
import {  AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class PostCommunityService {

  constructor(public afs: AngularFirestore) {

   }

   createPost(postCreate) {
    return new Promise<any>((resolve, reject) => {
      this.afs.collection('postCommunity').add({
        titulo: postCreate.titulo,
        ubicacion: postCreate.ubicacion,
        descripcion: postCreate.descripcion,
        categoria: postCreate.categoria,
        fecha: postCreate.fecha,
        contacto: postCreate.contacto
      })
      .then(
        res => resolve(res),
        err => reject(err)
      );
    });
  }


}
