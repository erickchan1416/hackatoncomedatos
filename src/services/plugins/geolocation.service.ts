import { Observable } from 'rxjs';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class GeolocationService {
    watch: Observable<Geoposition>;
    constructor( private geolocation: Geolocation ) { }

    getCurrentPosition() {
        return new Promise(
            (resolve, reject) => {
                let coords = {};
                this.geolocation.getCurrentPosition().then(
                    (resp) => {
                        coords = {
                            lat: resp.coords.latitude,
                            lng: resp.coords.longitude
                        };
                        resolve(coords);
                   }).catch((error) => {
                        console.log('Error getting location', error);
                        reject(error);
                   });
            }
        );
    }

    watchPosition(): Observable<Geoposition> {
        this.watch = this.geolocation.watchPosition();

        return this.watch;
    }
}
