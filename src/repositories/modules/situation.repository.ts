import { Situation } from './../../utils/interfaces/modules/situation.interface';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class SituationRepository {
    private situationsCollection: AngularFirestoreCollection<Situation>;
    situations: Observable<Situation[]>;

    constructor(private afs: AngularFirestore) {
        console.log('Situation Repository Ready');
        this.situationsCollection = this.afs.collection<Situation>('situations');
    }

    getSituations(): Observable<Situation[]> {
        this.situations = this.situationsCollection.valueChanges();
        return this.situations;
    }

    async addSituation(situation: Situation): Promise<any> {
        return new Promise(
            async (resolve, reject) => {
            const id = this.afs.createId();
            situation._id = id;
            await this.situationsCollection.doc(id).set(situation).then(
                () => resolve(true),
                err => reject(err)
            );
        });
    }
}
