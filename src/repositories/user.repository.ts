import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class UserRepository {
    users: Observable<any[]>;
    constructor(db: AngularFirestore) {
      this.users = db.collection('users').valueChanges();

      this.users.forEach(element => {
          console.log('Element', element);
      })
    }
}