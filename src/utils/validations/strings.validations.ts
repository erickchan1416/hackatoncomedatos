import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class StringValidationsController {
    constructor() {}

    // Este metodo valida si la cadena no tiene espacios
    withOutSpaces(str = ''): boolean {
        let noValid = /\s/;

        if (noValid.test(str)) {
            console.log('Este string no puede tener espacios');
            return false;
        } else {
            return true;
        }
    }

    // Este metodo valida si la cadena es un numero
    isNumber(str = ''): boolean {
        let valid = /^([0-9])*$/;

        if (valid.test(str)) {
            return true;
        } else {
            console.log('Este string no es un numero');
            return false;
        }
    }
}
