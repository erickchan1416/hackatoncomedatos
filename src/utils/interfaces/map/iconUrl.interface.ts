export interface IIconUrl {
    url: string;
    scaledSize?: {
        width?: number;
        height?: number; 
    };
};
