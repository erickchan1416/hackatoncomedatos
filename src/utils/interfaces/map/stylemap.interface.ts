export interface IStyleMap {
    featureType?: string;
    elementType?: string;
    stylers?: [];
}