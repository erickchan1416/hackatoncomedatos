import { IBtnCenterMap } from './btncentermap.interface';
export interface IMapConfigurations {
    addMarker?: boolean;
    markOverAnimation?: boolean;
    markOutAnimation?: boolean;
    streetViewControl?: boolean;
    buttonCenter?: IBtnCenterMap;
    buttonAdd?: IBtnCenterMap;
    showSearchBar?: boolean;
    showMarkSearch?: boolean;
}
