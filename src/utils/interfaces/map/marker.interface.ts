import { IIconUrl } from './iconUrl.interface';

export interface IMarker {
	id: number | string;
	lat: number;
	lng: number;
	label?: string;
	draggable?: boolean;
    animation?: 'DROP' | 'BOUNCE' | '';
	iconUrl?: IIconUrl;
	visible?: boolean;
}
