export interface IBtnCenterMap {
    activated: boolean;
    icon: string;
    activatedCenterMethod?: boolean;
}
