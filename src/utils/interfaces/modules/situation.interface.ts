export interface Situation {
    _id: string | number;
    lat: number;
    lng: number;
    date?: Date;
}
