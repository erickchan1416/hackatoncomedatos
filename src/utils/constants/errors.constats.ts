
export const ERRPERMITS = {
    2001: 'No se pudieron obtener los permisos necesarios', // El usuario no otorgo el permiso necesario para acceder a la funcionalidad
};

export const ERRCALL = {
    3001: 'Numero de telefono invalido', // La cadena es un número de telefono invalido
};

export const ERRPLATFORM = {
    5601: 'Está función solamente se puede ejecutar en un dispositivo móvil', // Requiere de un dispositivo móvil para ejecutar está opción pero se encuentra en web
    5602: 'Está función solamente se puede ejecutar en web', // Requiere de una aplicación web para ejecutar la opción pero se encuentra en un dispositivo móvil
};