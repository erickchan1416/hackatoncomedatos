// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAFRKBO3mWN-pI5ooNhBWQ9jPKVW7sXWp0',
    authDomain: 'comedatos.firebaseapp.com',
    databaseURL: 'https://comedatos.firebaseio.com',
    projectId: 'comedatos',
    storageBucket: 'comedatos.appspot.com',
    messagingSenderId: '176388938866',
    appId: '1:176388938866:web:8fefeeb939498edc5d36eb'
  },
  apiMapKey: 'AIzaSyDG8QKfRDNarnc8v2UMc09yc_5OlQcWq2w',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
