import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, LoadingController } from '@ionic/angular';
import { PostCommunityService } from '../../../../services/post-community.service';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.page.html',
  styleUrls: ['./post-create.page.scss'],
})
export class PostCreatePage implements OnInit {

  title = 'Formulario';
  postCreate = {
    titulo: '',
    ubicacion: '',
    descripcion: '',
    categoria: '',
    fecha: '',
    contacto: '',
  };
  constructor(public navCtrl: NavController,
              private modalCtrl: ModalController,
              public loadingCtrl: LoadingController,
              private firebaseService: PostCommunityService
        ) { }

  ngOnInit() {
  }
  closeModal() {
    this.modalCtrl.dismiss();
  }

  send() {
    this.firebaseService.createPost(this.postCreate)
    .then(
      res => {
        console.log("Se ha guardado el registro");
      
      }
    )
  }
}
