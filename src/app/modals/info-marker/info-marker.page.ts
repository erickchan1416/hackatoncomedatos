import { IMarker } from './../../../utils/interfaces/map/marker.interface';
import { ModalController } from '@ionic/angular';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-info-marker',
  templateUrl: './info-marker.page.html',
  styleUrls: ['./info-marker.page.scss'],
})
export class InfoMarkerPage implements OnInit {
  @Input() data: IMarker;
  title = '';

  post;
  oculto = 150;
  loader1 = true;
  loader2 = true;

  userData = {
    category: '',
    id: '',
    potsId: '',
    sol: false,
    univerty: ''
  };
  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
    this.loader1 = false;
    this.loader2 = false;
    console.log('data', this.data);
    this.title = this.data.label;
  }

  closeModal() {
    this.modalCtrl.dismiss();
  }

  goToDirection() {
    this.modalCtrl.dismiss({
      marker: this.data
    });    
  }

}
