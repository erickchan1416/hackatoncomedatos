import { environment } from './../../environments/environment';
import { HeaderModalComponent } from './header-modal/header-modal.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { MapComponent } from './map/map.component';
import { LoaderComponent } from './loader/loader.component';

import { ComunidadComponent } from './comunidad/comunidad/comunidad.component';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';


@NgModule({
  declarations: [
    HeaderComponent,
    HeaderModalComponent,
    MapComponent,
    LoaderComponent,
    ComunidadComponent,
  ],
  exports: [
    HeaderComponent,
    HeaderModalComponent,
    MapComponent,
    LoaderComponent,
    ComunidadComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule,
    AgmCoreModule.forRoot({
      apiKey: environment.apiMapKey,
      libraries: ['places']
    }),
    AgmDirectionModule,
    GooglePlaceModule
  ]
})
export class ComponentsModule { }