import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header-modal',
  templateUrl: './header-modal.component.html',
  styleUrls: ['./header-modal.component.scss'],
})
export class HeaderModalComponent implements OnInit {

  @Input() title;
  @Output() closeModal = new EventEmitter();

  constructor() { }

  ngOnInit() {}

  exitModal() {
    this.closeModal.emit();
  }

}
