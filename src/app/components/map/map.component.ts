import { Situation } from './../../../utils/interfaces/modules/situation.interface';
import { SituationService } from './../../../services/modules/situation.service';
import { MessagesController } from './../../../utils/messages/messages';
import { GeolocationService } from './../../../services/plugins/geolocation.service';
import { Component, OnInit, Input, EventEmitter, Output, ViewChild, AfterViewInit } from '@angular/core';
import { IMarker } from 'src/utils/interfaces/map/marker.interface';
import { MouseEvent, AgmMap, LatLngBounds, MapsAPILoader } from '@agm/core';
import { IMapConfigurations } from 'src/utils/interfaces/map/mapconfigurations.interface';
import { DataService } from 'src/services/data.service';
import { IStyleMap } from 'src/utils/interfaces/map/stylemap.interface';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

declare var google: any;

@Component({
  selector: 'app-map-component',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit, AfterViewInit {

  @ViewChild('AgmMap', {static: false}) agmMap: AgmMap;
  @ViewChild('placesRef', {static: false}) placesRef: GooglePlaceDirective;

  @Input() zoom = 12;

  @Input() myMarker: IMarker = {
    id: 1,
    lat: 21.160912,
    lng: -86.851218,
  };

  @Input() markers: IMarker[] = [];

  @Output() clickedMarker = new EventEmitter();
  @Output() mapClicked = new EventEmitter();
  @Output() markerDragEnd = new EventEmitter();
  @Output() markerOver = new EventEmitter();
  @Output() markerOut = new EventEmitter();
  @Output() moreInfoMarker = new EventEmitter();

  @Input() mapConfigurations: IMapConfigurations = {};

  styles: IStyleMap[] = [];

  streetViewControl = false;
  btnCenterIcon = 'locate';
  btnCenter = false;
  lat = 21.160912;
  lng = -86.851218;
  markerAnimation: IMarker[] = [];

  direction = false;
  origin = { lat: 24.799448, lng: 120.979021 };
  destination = { lat: 24.799524, lng: 120.975017 };
  renderOptions: {
    suppressMarkers: true,
  };

  options = {
    types: [],
    componentRestrictions: { country: 'MX' }
  };

  markSearch: IMarker = {
    id: 0,
    lat: 21.1443449,
    lng: -86.8457164,
    visible: false,
    draggable: false,
    animation: 'DROP',
  };

  btnAdd = false;
  btnAddIcon = 'ADD';
  bounds: any = false;
  showSearchBar = false;
  showMarkSearch = false;
  addSituation = false;
  btnCloseAdd = false;
  constructor(private dataService: DataService,
              private mapsAPILoader: MapsAPILoader,
              private messagesCtrl: MessagesController,
              private _situationService: SituationService) { }

  handleAddressChange(address: Address) {
    if (this.showMarkSearch) {
      this.markSearch.visible = true;
      console.log('add', address);
      console.log(
        address.geometry.location.lat(),
        address.geometry.location.lng()
      );

      this.markSearch = {
        id: address.id,
        lat: address.geometry.location.lat(),
        lng: address.geometry.location.lng(),
        visible: false,
      };

      setTimeout(() => {
        this.markSearch.visible = true;
      });

    }
    this.setBounds(address.geometry.location.lat(), address.geometry.location.lng());
  }

  setBounds(lat, lng) {
		this.mapsAPILoader.load().then(() => {
      this.bounds = new google.maps.LatLngBounds();
      if (this.showMarkSearch) {
          this.bounds.extend( new google.maps.LatLng( 21.160912, -86.851218));
        }
			   this.bounds.extend( new google.maps.LatLng( lat, lng));
		});
	}

  ngOnInit() {
    this.getConfigurations();

    this.lat = this.myMarker.lat;
    this.lng = this.myMarker.lng;

    console.log(this.myMarker);

    this.dataService.getStyleMap().subscribe(
      (data: IStyleMap[]) => {
        this.styles = data;
      }
    );
  }

  centerMapMarkers() {
    console.log('Centrando Mapa');
    this.agmMap.mapReady.subscribe(map => {
      const bounds: LatLngBounds = new google.maps.LatLngBounds();
      for (const mm of this.markers) {
        bounds.extend(new google.maps.LatLng(mm.lat, mm.lng));
      }
      map.fitBounds(bounds);
    });
  }

  ngAfterViewInit() {
    this.markers.push(this.myMarker);
    this.centerMapMarkers();
    this.agmMap.mapReady.subscribe(map => {

      const strictBounds = new google.maps.LatLngBounds(
        new google.maps.LatLng(20.887532, -87.199887),
        new google.maps.LatLng(21.270265, -86.704869));

      google.maps.event.addListener(map, 'dragend', () => {
        if (strictBounds.contains(map.getCenter())) { return; }

        let c, x, y, maxX, maxY, minX, minY;

        c = map.getCenter();
        x = c.lng();
        y = c.lat();
        maxX = strictBounds.getNorthEast().lng();
        maxY = strictBounds.getNorthEast().lat();
        minX = strictBounds.getSouthWest().lng();
        minY = strictBounds.getSouthWest().lat();

        if (x < minX) { x = minX; }
        if (x > maxX) { x = maxX; }
        if (y < minY) { y = minY; }
        if (y > maxY) { y = maxY; }

        map.setCenter(new google.maps.LatLng(y, x));
      });

      google.maps.event.addListener(map, 'zoom_changed', () => {
        if (map.getZoom() < 11) { map.setZoom(11); }
      });

    });

  }

  getConfigurations() {
    if (this.mapConfigurations.buttonCenter !== null && this.mapConfigurations.buttonCenter !== undefined ) {
      this.btnCenter = this.mapConfigurations.buttonCenter.activated;

      if (this.mapConfigurations.buttonCenter.icon !== null && this.mapConfigurations.buttonCenter.icon !== undefined && this.mapConfigurations.buttonCenter.icon !== '') {
        this.btnCenterIcon = this.mapConfigurations.buttonCenter.icon;
      }
    }

    if (this.mapConfigurations.buttonAdd !== null && this.mapConfigurations.buttonAdd !== undefined ) {
      this.btnAdd = this.mapConfigurations.buttonAdd.activated;

      if (this.mapConfigurations.buttonAdd.icon !== null && this.mapConfigurations.buttonAdd.icon !== undefined && this.mapConfigurations.buttonAdd.icon !== '') {
        this.btnAddIcon = this.mapConfigurations.buttonAdd.icon;
      }
    }

    if (this.mapConfigurations.streetViewControl) {
      this.streetViewControl = this.mapConfigurations.streetViewControl;
    }

    if (this.mapConfigurations.showSearchBar) {
      this.showSearchBar = this.mapConfigurations.showSearchBar;
    }

    if (this.mapConfigurations.showMarkSearch) {
      this.showMarkSearch = this.mapConfigurations.showMarkSearch;
    }

    if (this.myMarker.lat && this.myMarker.lng) {
      console.log('Tiene latitue');
      this.lat = this.myMarker.lat;
      this.lng = this.myMarker.lng;
    }
  }

  clickMarker(marker: IMarker) {
    const res = this.markerAnimation.find( x => x.id === marker.id);
    if (res === null || res === undefined) {
      marker.animation = null;
      marker.animation = 'BOUNCE';
      this.markerAnimation.push(marker);
      console.log(this.markerAnimation);
      return;
    }

    if (marker.animation !== null) {
      marker.animation = null;
    } else {
      marker.animation = 'BOUNCE';
    }
    this.clickedMarker.emit({
      marker
    });
  }

  hideAnimationMarker(marker: IMarker) {
    marker.animation = null;
  }

  mapClick($event: MouseEvent) {
    if (this.mapConfigurations.addMarker) {
      this.markers.push({
        id: this.markers.length + 1,
        lat: $event.coords.lat,
        lng: $event.coords.lng,
        draggable: true,
        animation: 'DROP'
      });
    }

    this.mapClicked.emit({$event});
  }

  markDragEnd(m: IMarker, $event: MouseEvent) {
    this.markSearch.lat = $event.coords.lat;
    this.markSearch.lng = $event.coords.lng;
    console.log( m, $event);

    this.markerDragEnd.emit({m, $event});
  }

  markOver(m: IMarker) {
    if (this.mapConfigurations.markOverAnimation) {
      m.animation = 'BOUNCE';
    }
    this.markerOver.emit({m});
  }

  markOut(m: IMarker) {
    if (this.mapConfigurations.markOutAnimation) {
      m.animation = '';
    }
    this.markerOut.emit({
      m
    });
  }

  centerMarker() {
    this.lat = 0;
    this.lng = 0;
    this.markSearch.lat = 0;
    this.markSearch.lng = 0;
    this.markSearch.draggable = false;

    setTimeout (() => {
      this.lat = this.myMarker.lat;
      this.lng = this.myMarker.lng;
      this.markSearch.lat = this.myMarker.lat;
      this.markSearch.lng = this.myMarker.lng;
      this.markSearch.draggable = true;

    });
  }

  showMoreInfoMarker(m: IMarker) {
    this.moreInfoMarker.emit(m);
  }

  showDirection(n: IMarker, m: IMarker) {
    this.origin = {
      lat: n.lat,
      lng: n.lng,
    };

    this.destination = {
      lat: m.lat,
      lng: m.lng
    };
    this.direction = true;

  }

  closeDirection() {
    this.direction = false;
    this.centerMarker();
  }

  selectMarkerSituation() {
    this.btnCenter = false;
    this.btnCenter = true;
    this.btnAdd = false;
    this.addSituation = true;
    this.btnCloseAdd = true;
    this.markSearch.draggable = true;

    this.markSearch.visible = true;
    this.showMarkSearch = true;
    this.markSearch.lat = this.myMarker.lat;
    this.markSearch.lng = this.myMarker.lng;
    this.setBounds(this.myMarker.lat, this.myMarker.lng);
    console.log(this.markSearch);
  }

  closeAddMarkerSituation() {
    this.btnAdd = true;
    this.addSituation = false;
    this.btnCloseAdd = false;
    this.markSearch.visible = false;
    this.showMarkSearch = false;
    this.markSearch.draggable = false;
  }

  async addMarkSituation() {
    this.messagesCtrl.presentAlertConfirm('Añadir', '¿Estas seguro que desear añadir este lugar como marcador?', 'No', 'Sí').then(
      res => {
        console.log('res', res);
        if (res) {
          console.log(this.markSearch);
          this.markSearch.visible = true;
          // this.markers.push({
          //   id: 0,
          //   lat: this.markSearch.lat,
          //   lng: this.markSearch.lng,
          //   visible: true,
          //   iconUrl: {
          //     url: '/assets/icon/placeholder (1).png',
          //     scaledSize: {
          //       height: 45,
          //       width: 45,
          //     }
          //   }
          // });

          const newSituation: Situation = {
            _id: 0,
            lat: this.markSearch.lat,
            lng: this.markSearch.lng,
          };
          this._situationService.addSituation(newSituation).then(
            data => {
              if (data) {
                this.closeAddMarkerSituation();
              }
            }
          );
          console.log(this.markers);
        }
      }
    );
  }
}
