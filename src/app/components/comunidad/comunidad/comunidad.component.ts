import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { PostCreatePage } from '../../../modals/postAyuda/post-create/post-create.page';
import { PostCommunityService } from '../../../../services/post-community.service';

@Component({
  selector: 'app-comunidad',
  templateUrl: './comunidad.component.html',
  styleUrls: ['./comunidad.component.scss'],
})
export class ComunidadComponent implements OnInit {

  constructor(public navCtrl: NavController, public modalController: ModalController, private postCommunity: PostCommunityService) { }

  ngOnInit() {
  }
  async abrirModal() {
    const modal = await this.modalController.create({
     component: PostCreatePage
   });
    return modal.present();
 }



}
