import { InfoMarkerPage } from './../../modals/info-marker/info-marker.page';
import { InfoMarkerPageModule } from './../../modals/info-marker/info-marker.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MapPage } from './map.page';
import { ComponentsModule } from 'src/app/components/components.module';

const routes: Routes = [
  {
    path: '',
    component: MapPage
  }
];

@NgModule({
  entryComponents:[
    InfoMarkerPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule,
    InfoMarkerPageModule
  ],
  declarations: [MapPage]
})
export class MapPageModule {}
