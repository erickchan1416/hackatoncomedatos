import { SituationService } from './../../../services/modules/situation.service';
import { MessagesController } from './../../../utils/messages/messages';
import { GeolocationService } from './../../../services/plugins/geolocation.service';
import { InfoMarkerPage } from './../../modals/info-marker/info-marker.page';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { IMarker } from 'src/utils/interfaces/map/marker.interface';
import { IMapConfigurations } from 'src/utils/interfaces/map/mapconfigurations.interface';
import { ModalController } from '@ionic/angular';
import { MapComponent } from 'src/app/components/map/map.component';
import { Subscription } from 'rxjs';
import { Situation } from 'src/utils/interfaces/modules/situation.interface';

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {

  @ViewChild(MapComponent, {static: false})
  private mapComponent: MapComponent;

  zoom = 8;

  myMarker: IMarker = {
    id: 1,
    lat:  21.160912,
    lng:  -86.851218,
    animation: 'DROP',
    draggable: false,
    iconUrl: {
      scaledSize: {
        width: 50,
        height: 50,
      },
      url: '/assets/icon/location.png'
    }
  };

  markers: IMarker[] = [];

  mapConfiguration: IMapConfigurations;

  private _postionListener: Subscription = new Subscription();

  constructor(private modalCtrl: ModalController,
              private _messagesCtrl: MessagesController,
              private _geolocationService: GeolocationService,
              private _situationService: SituationService) { }

  ngOnInit() {
    this.mapConfiguration = {
      addMarker: false,
      markOutAnimation: false,
      markOverAnimation: false,
      streetViewControl: false,
      buttonCenter: {
        activated: true,
        icon: '',
      },
      buttonAdd: {
        activated: true,
        icon: '',
      },
      showSearchBar: true,
      showMarkSearch: false,
    };

    this._situationService.getSituations().subscribe(
      (data: Situation[]) => {
        this.markers.length = 0;
        data.forEach(element => {
          const a: IMarker = {
            id: element._id,
            lat: element.lat,
            lng: element.lng,
            label: 'Hola a todos',
            animation: 'DROP',
            visible: true,
            iconUrl: {
              scaledSize: {
                width: 40,
                height: 40,
              },
              url: '/assets/icon/placeholder (1).png'
            }
          };
          this.markers.push(a);
        });
      }
    );
  }

  ionViewWillLeave() {
    try {
      this._postionListener.unsubscribe();
    } catch (error) {
      console.log(error);
    }
  }

  ionViewWillEnter() {
    this.watchMyPosition();
  }

  watchMyPosition() {
    this._postionListener = this._geolocationService.watchPosition().subscribe(
      (data) => {
        console.log(data);
        if (data) {
          this.myMarker.lat = 0;
          this.myMarker.lng = 0;

          setTimeout(() => {
            this.myMarker.lat = data.coords.latitude;
            this.myMarker.lng = data.coords.longitude;
          });
        } else {
          this._messagesCtrl.presentAlertOk('Error', 'Ocurrió un error al obtener tu ubicación');
        }
      }
    );
  }

  clikMarker($event) {
    console.log('marker', $event);
  }

  mapClick($event) {
    console.log('map', $event);
  }

  markerDragEnd($event) {
    console.log('dragend', $event);
  }

  async showMoreInfoMarker($event) {
    const modal = await this.modalCtrl.create({
      component: InfoMarkerPage,
      componentProps: {
        data: $event
      }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    console.log('data', data);

    if (data !== undefined && data !== null) {
      const markerDestination: IMarker =  this.markers.find(x => x.id === data.marker.id);
      this.mapComponent.hideAnimationMarker(markerDestination);
      this.mapComponent.showDirection(this.myMarker, markerDestination);
    }
  }

}
