import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  tagSelected = 'emergency';
  constructor(private router: Router) {
    this.selectTagByUrl();
  }

  selectTag(tag) {
    this.tagSelected = tag;
  }

  selectTagByUrl() {
    switch (this.router.url) {
      case '/tabs/map':
        this.tagSelected = 'map';
        break;

        case '/tabs/emergency':
        this.tagSelected = 'emergency';
      
        break;    
      case '/tabs/community':
        this.tagSelected = 'community';
      
        break;

      default:
        break;
    }
  }
}
