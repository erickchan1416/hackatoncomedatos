import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from 'src/utils/guards/candeactivate.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  { path: 'map', loadChildren: './pages/map/map.module#MapPageModule' },
  { path: 'community', loadChildren: './pages/community/community.module#CommunityPageModule' },  { path: 'post-create', loadChildren: './modals/postAyuda/post-create/post-create.module#PostCreatePageModule' },

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
